# Quake log parser

## Task 1 - Construa um parser para o arquivo de log games.log e exponha uma API de consulta.

O arquivo games.log é gerado pelo servidor de quake 3 arena. Ele registra todas as informações dos jogos, quando um jogo começa, quando termina, quem matou quem, quem morreu pq caiu no vazio, quem morreu machucado, entre outros.

O parser deve ser capaz de ler o arquivo, agrupar os dados de cada jogo, e em cada jogo deve coletar as informações de morte.

### Observações

1. Quando o `<world>` mata o player ele perde -1 kill.
2. `<world>` não é um player e não deve aparecer na lista de players e nem no dicionário de kills.
3. `total_kills` são os kills dos games, isso inclui mortes do `<world>`.

----------

## Task 2 - Após construir o parser construa uma API que faça a exposição de um método de consulta que retorne um relatório de cada jogo.

----------

## Requisitos

1. Use a linguagem que você tem mais habilidade (temos preferência por nodejs, java ou python, mas pode ser usado qualquer linguagem desde que explicado a prefência).
2. Faça testes unitários, suite de testes bem organizados. (Dica. De uma atenção especial a esse item!)
3. Use git e tente fazer commits pequenos e bem descritos.
4. Faça pelo menos um README explicando como fazer o setup, uma explicação da solução proposta, o mínimo de documentação para outro desenvolvedor entender seu código
5. Siga o que considera boas práticas de programação, coisas que um bom desenvolvedor olhe no seu código e não ache "feio" ou "ruim".
6. Após concluir o teste, envie o link do repositório com a solução commitado no github respondendo o email de contato.

HAVE FUN :)

----------

## Tecnologias e desenvolvimento do QuakeParser 
Foi utilizado como pedido o Java (Spring) que é uma das tecnologias que tenho experiência para desenvolver projetos REST.

### Parser
O Parser 

** Como executar :**

1) Fazer clone do repositório. 
	git clone https://lucasgustavo088@bitbucket.org/lucasgustavo088/quake-log-parser-java.git

2) Abrir o eclipse

3) File -> import -> Existing Maven Project. Selecionar o caminho do repositório.

4) Run QuakelogparserApplication.java

### API
Com a API é possível obter dados de jogos passando no segundo parâmetro o ID do mesmo. Caso esse valor esteja vazio. Retornará todos os jogos

** Chamadas da API:**
	
	1 - Retorna um json com todos os jogos encontrados no arquivo
   	GET http://localhost:8080/api/
	
	2 - Retorna os dados do jogo 2
	GET http://localhost/gamelog/api/2

### Test

** Como realizar os testes **

	1 - Run QuakelogparserApplication.java (A url http://localhost:8080 deverá funcionar)
	2 - Run as JUnit a classe TestQuakeLogParser dentro do pacote de test

Obs: foram criados métodos dessa classe para testar todos os dados REST do método GET do parser e da Api.
	
\O/
Thank you!

