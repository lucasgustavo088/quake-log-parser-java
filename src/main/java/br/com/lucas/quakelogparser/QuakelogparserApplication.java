package br.com.lucas.quakelogparser;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class QuakelogparserApplication {

	public static void main(String[] args) {
		SpringApplication.run(QuakelogparserApplication.class, args);
	}

}
