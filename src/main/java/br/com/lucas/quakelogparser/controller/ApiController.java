package br.com.lucas.quakelogparser.controller;

import java.io.IOException;
import java.util.ArrayList;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.ResponseBody;

import br.com.lucas.quakelogparser.model.Game;

@Controller
public class ApiController {
	
	@GetMapping(path = {"/api/{id}", "/api/", "/api"}, produces = "application/json; charset=UTF-8")
	public @ResponseBody ArrayList<Game> api(@PathVariable(name = "id", required = false) Integer id) throws IOException {
		ParserLog parseLog = new ParserLog();
		
		if(id == null) {
			return parseLog.jsonGames;
		} else {
			ArrayList<Game> gameJson = new ArrayList<Game>();
			for(Game game : parseLog.jsonGames) {
				if(id == game.getId()) {
					gameJson.add(game);
				}
				
			}
			
			return gameJson;
		}
		
	    
	}
}
