package br.com.lucas.quakelogparser.controller;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;

import br.com.lucas.quakelogparser.model.Game;
import br.com.lucas.quakelogparser.model.Line;

public class ParserLog {

	public Gson gson;
	
	public String filePath;
	public Game game = null;
	public int quantidadeJogos = 0;
	public Map<String, Integer> kills = new HashMap<String,Integer>();
	public Map<String, Integer> motivoKill = new HashMap<String,Integer>();
	public ArrayList<Game> jsonGames = new ArrayList<Game>(); 
	
	public ParserLog() throws IOException {
		
		this.startAndRead();
	}
	
	private void startAndRead() throws IOException {
		/* Verify if file exists */
		
		File gamesLogFile = new File(
			getClass().getClassLoader().getResource("games.log").getFile()
		);
		
		try {
	        new java.io.FileInputStream(gamesLogFile);
	    } catch (java.io.FileNotFoundException e) {
	        System.out.println("Nao foi possível abrir o arquivo para leitura");
	    }
		
		try {
			this.read(new FileReader(gamesLogFile));
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
	}
	
	private void read(FileReader fileReader) throws IOException {
		BufferedReader reader = new BufferedReader(fileReader);
		
		String readLine = "";
		ArrayList<String> linhas = new ArrayList<String>();
		
        while ((readLine = reader.readLine()) != null) {
        	linhas.add(readLine);
        }
        
        for(String linhaAtual : linhas) {
        	Line linhaParseada = parserLine(linhaAtual);
        	
        	if(linhaParseada.getComando().equals("InitGame")) {
        		this.initGame(linhaParseada);
        	} else if(linhaParseada.getComando().equals("ClientUserinfoChanged")) {
        		this.clientUserinfoChanged(linhaParseada);
        	} else if(linhaParseada.getComando().equals("Kill")) {
        		this.kill(linhaParseada);
        	} else if(
        			linhaParseada.getComando().equals("shutdownGame")
        			|| linhaParseada.getComando().equals("------------------------------------------------------------")
        			) {
        		this.shutdownGame(linhaParseada);
        	}
        }
		reader.close();		
	}
	
	public void kill(Line linhaParseada) {
		this.game.addKill();
		String[] value = linhaParseada.getParametros().split(":", 2);
		String[] value2 = value[1].split("killed");
		String pKiller = value2[0].trim();
		String[] value3 = value2[1].trim().split(" by ");
		String pKilled = value3[0].trim();
		String mod = value3[1].trim();

        /* Adicionar jogador que matou e jogador que morreu */
        this.setKill(pKiller, pKilled);

        /* Adicionar kill por siginificado */
        this.motivoKill.put(
        		mod, 
        		(this.motivoKill.containsKey(mod) ? this.motivoKill.get(mod) : 0) + 1
        	);
	}
	
	private void shutdownGame(Line linhaParseada) throws JsonProcessingException {
		if(this.game != null) {
			/* Adicionar kills no game */
			this.game.setKills(this.kills);
			
			/* Adicionar kills por significado no game */
			this.game.setMotivoKill(this.motivoKill);
			
			/* Colocar o horario final */
			this.game.setEndTime(linhaParseada.getHorario());	
			
			jsonGames.add(game);
			this.game = null;
		}
	}
	
	private void setKill(String pKiller, String pKilled) {
        if (pKiller.equals("<world>")) {
            /* Remove uma kill do player que se matou */
        	if(!this.kills.containsKey(pKilled)) {
        		this.kills.put(pKilled, -1);
        	} else {
        		this.kills.put(pKilled, this.kills.get(pKilled) - 1);
        	}
        } else if (pKiller.equals(pKilled)) {
        	/* Se o player se matou */
        	if(!this.kills.containsKey(pKilled)) {
        		this.kills.put(pKilled, -1);
        	} else {
        		this.kills.put(pKilled, this.kills.get(pKilled) - 1);
        	}
        } else {
        	/* Se o jogador matou o oponente */
        	if(!this.kills.containsKey(pKilled)) {
        		this.kills.put(pKilled, 1);
        	} else {
        		this.kills.put(pKilled, this.kills.get(pKilled) + 1);
        	}
        }
    }
	
	public void clientUserinfoChanged(Line linhaParseada) {
		String[] jogador = linhaParseada.getParametros().split("t", 2);
		String[] jogadorSplited = jogador[0].split(" n", 2);

        /* Verificar se o nome do jogador existe */
		boolean jogadorExiste = false;
		for(String jogadorGame : this.game.getPlayers()) {
			if(jogadorGame.equals(jogadorSplited[1])) {
				jogadorExiste = true;
			}
		}
		
		if(!jogadorExiste) {
			this.game.addPlayer(jogadorSplited[1]);
		}
	}
	
	public void initGame(Line linhaParseada) {
		this.game = new Game();
		this.motivoKill = new HashMap<String,Integer>();
		
		this.kills = new HashMap<String,Integer>();
		
		this.quantidadeJogos++;
		this.game.setStartTime(linhaParseada.getHorario());
		this.game.setId(this.quantidadeJogos);
	}
	
	public Line parserLine(String linha) {
		Line line = new Line();

		if(!linha.equals("")) {
			linha = linha.trim();
			String parametros[] = linha.split(":", 3);
			
			String horarios[]  = parametros[0].split(" ", 2);
			String horario = "";
			if(horarios.length > 1 && horarios[1] != null) {
				horario = horarios[1];
			} else {
				horario = horarios[0];
			}
			horario = (horario + ":" + parametros[1]).trim();
			
			String[] horarioComando = horario.split(" ", 2);
			
			line.setParametros(parametros.length > 2 && parametros[2] != null ? parametros[2] : "");
			line.setHorario(horarioComando[0]);
			line.setComando(horarioComando[1]);
		}
		
		return line;
	}
	
	
}	
