package br.com.lucas.quakelogparser.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.Map;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import br.com.lucas.quakelogparser.model.Game;

@Controller
public class ParserLogController {

	@GetMapping("/api")
	public ModelAndView api(Map<String, Object> model) {
		ModelAndView mv = new ModelAndView("api");

		return mv;
	}

	
	@GetMapping(path = "/", produces = "application/json; charset=UTF-8")
	public @ResponseBody ArrayList<Game> index() throws IOException {
		ParserLog parseLog = new ParserLog();
	    return parseLog.jsonGames;
	}

}
