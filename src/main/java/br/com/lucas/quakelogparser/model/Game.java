package br.com.lucas.quakelogparser.model;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class Game {
	
	public int id;
	public String startTime;
	public String endTime;
	public int totalKills;
	public ArrayList<String> players = new ArrayList<String>(); 
	public Map<String, Integer> kills = new HashMap<String,Integer>();
	public Map<String, Integer> motivoKill = new HashMap<String,Integer>();
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getStartTime() {
		return startTime;
	}
	public void setStartTime(String startTime) {
		this.startTime = startTime;
	}
	public String getEndTime() {
		return endTime;
	}
	public void setEndTime(String endTime) {
		this.endTime = endTime;
	}
	public int getTotalKills() {
		return totalKills;
	}
	public void setTotalKills(int totalKills) {
		this.totalKills = totalKills;
	}
	public ArrayList<String> getPlayers() {
		return players;
	}
	public void setPlayers(ArrayList<String> players) {
		this.players = players;
	}
	public void addPlayer(String player) {
		this.players.add(player);
	}
	public void addKill() {
		this.setTotalKills(this.getTotalKills()+ 1);
	}
	public Map<String, Integer> getKills() {
		return kills;
	}
	public void setKills(Map<String, Integer> kills) {
		this.kills = kills;
	}
	public Map<String, Integer> getMotivoKill() {
		return motivoKill;
	}
	public void setMotivoKill(Map<String, Integer> motivoKill) {
		this.motivoKill = motivoKill;
	}
	
	
	
}
