package br.com.lucas.quakelogparser.model;

import java.util.ArrayList;
import java.util.List;

public class Line {
	
	private String comando;
	private String parametros;
	private String horario;
	public String getComando() {
		return comando;
	}
	public void setComando(String comando) {
		this.comando = comando;
	}
	
	public String getHorario() {
		return horario;
	}
	public void setHorario(String horario) {
		this.horario = horario;
	}
	public String getParametros() {
		return parametros;
	}
	public void setParametros(String parametros) {
		this.parametros = parametros;
	}
	@Override
	public String toString() {
		return "Line [comando=" + comando + ", parametros=" + parametros + ", horario=" + horario + "]";
	}
	
	
	
	
}
