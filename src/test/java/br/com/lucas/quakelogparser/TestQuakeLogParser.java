package br.com.lucas.quakelogparser;


import java.io.File;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.skyscreamer.jsonassert.JSONAssert;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.util.Assert;


@RunWith(SpringRunner.class)
@SpringBootTest(classes = QuakelogparserApplication.class)
public class TestQuakeLogParser {


	TestRestTemplate restTemplate = new TestRestTemplate();

	HttpHeaders headers = new HttpHeaders();
	
	/* API */
	@Test
	public void testGetApiAllGames() throws Exception {
		HttpEntity<String> entity = new HttpEntity<String>(null, headers);
		
		ResponseEntity<String> response = restTemplate.exchange(
				createURLWithPort("/api/"),
				HttpMethod.GET, entity, String.class);
		
		JSONAssert.assertNotEquals("[]", response.getBody(), false);
	}
	
	@Test
	public void testGetAGame() throws Exception {
		HttpEntity<String> entity = new HttpEntity<String>(null, headers);
		
		ResponseEntity<String> response = restTemplate.exchange(
				createURLWithPort("/api/1"),
				HttpMethod.GET, entity, String.class);

		String expected = "[{\"id\":1,\"startTime\":\"0:00\",\"endTime\":\"20:37\",\"totalKills\":0,\"players\":[\"\\\\Isgalamido\\\\\"],\"kills\":{},\"motivoKill\":{}}]";
		JSONAssert.assertEquals(expected, response.getBody(), false);
	}
	
	@Test
	public void testApiIdNotExists() throws Exception {
		HttpEntity<String> entity = new HttpEntity<String>(null, headers);
		
		ResponseEntity<String> response = restTemplate.exchange(
				createURLWithPort("/api/111111"),
				HttpMethod.GET, entity, String.class);

		String expected = "[]";
		JSONAssert.assertEquals(expected, response.getBody(), false);
	}
	
	/* Parser */
	@Test
	public void testParserGetAll() throws Exception {
		HttpEntity<String> entity = new HttpEntity<String>(null, headers);
		
		ResponseEntity<String> response = restTemplate.exchange(
				createURLWithPort("/"),
				HttpMethod.GET, entity, String.class);
		
		JSONAssert.assertNotEquals("[]", response.getBody(), false);
	}
	
	@Test
	public void testParserGet404Page() throws Exception {
		HttpEntity<String> entity = new HttpEntity<String>(null, headers);
		
		ResponseEntity<String> response = restTemplate.exchange(
				createURLWithPort("/404"),
				HttpMethod.GET, entity, String.class);
		
		System.out.println(response.getBody());
		JSONAssert.assertNotEquals("[]", response.getBody(), false);
	}
	
	@Test
	public void testFileGameExists() throws Exception {
		File gamesLogFile = new File(
				getClass().getClassLoader().getResource("games.log").getFile()
			);
		
		Assert.isTrue(gamesLogFile != null, "");
	}
	
	private String createURLWithPort(String uri) {
		return "http://localhost:8080" + uri;
	}
}
